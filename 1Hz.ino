void einHz()
{
   Wire.begin();        // initialize the I2C/TWI interface
    delay(1000);         // delay as a precaution
    
    // write to the PCF8563
    Wire.beginTransmission(PCF8563address);
    Wire.write(0x0D);                  // point to the CLKOUT_CONTROL register
    Wire.write(0x83);                  // set the CLKOUT frequency to 1HZ
    Wire.endTransmission();
}
