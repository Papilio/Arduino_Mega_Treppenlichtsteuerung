/*************************************************************
Copyright (c) 2022 Papilio. All rights reserved.

This file is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU Lesser General Public License for more details.
***************************************************************/

#include <Wire.h>

#define PCF8563address 0x51  // alternativ: #define RTC_ADDR  (0xA2 >> 1)

const int einHz_in     =  4;
const int tasterLed    = 32;
const int eltako       = 33;
const int einHz_Takt   = 34;
const int Tr_test      = 35;
const int potiTimer    = A0;  // TrepLi Zeiteinstellung (Poti)
const int taster_licht = 22;
int eltakoState = HIGH;
     
int zaehler = 0;              // TrepLi (Sekunden)
int state = LOW;              // 1Hz-Takt (LOW-puls)
int laststate = LOW;          // TrepLi_Schalter Status
int lichtState = LOW;         // Trepli Satus (An oder aus)
int t = 0;                    // TrepLi_Zeitvorgabe
String sketchVersion;
///////////////////////////////////////////////////////////////////////
void setup() 
{
Wire.begin();
Serial.begin(9600);
delay(1000);
sketchVersion = (strrchr(__FILE__, '//') + 1);
delay(100); 
  Serial.println(sketchVersion);          // Sketchversion
  Serial.println(__DATE__);               // Erstellungsdatum
  Serial.println(__TIME__);               // Erstellungszeitpunkt
  Serial.println("Treppenhauslicht");     // Projektname
    
    pinMode(taster_licht, INPUT_PULLUP);  // TrepLi-Taster (Anschluss)
    pinMode(eltako, OUTPUT);
    pinMode(tasterLed, OUTPUT);
    pinMode(einHz_Takt, OUTPUT);
    pinMode(Tr_test, OUTPUT);
    
    digitalWrite(eltako,HIGH);       // TrepLi_Steuerung (Ein - Aus)
    digitalWrite(tasterLed,HIGH);
    digitalWrite(einHz_Takt,HIGH);
    digitalWrite(Tr_test,HIGH);
    
pinMode(einHz_in,INPUT_PULLUP);      // COT_PCF8563 (RTC-Module)

einHz();                      // Erzeugen des 1Hz-Signals für Timer
}
////////////////////////////////////////////////////////////////////////
void loop() 
{
 trepLi();                     // Treppenlichtsteuerung  
}
