void trepLi()
{        
   lichtState = (!(digitalRead(taster_licht))); // Lichttaster gedrückt = LOW --> lichtState = HIGH
         //Serial.println(lichtState);  
         //delay(3000);                 // Nur für lichtState Test notwendig
 
   if (!(digitalRead(taster_licht)) && (digitalRead(eltako) == LOW))
         digitalWrite( eltako , HIGH);
   
   while (lichtState == HIGH)  
  {
     digitalWrite(tasterLed , (digitalRead(taster_licht)));
     digitalWrite( eltako , LOW );       // Damit das Licht sofort einschaltet
     digitalWrite(tasterLed , HIGH);
     digitalWrite(einHz_Takt , digitalRead(einHz_in));
     t = (analogRead(potiTimer) / 30);   // "analogRead": Werte von 0 bis 1023
     //Serial.print(t);                  // Z. B. Wert 630 / 30 = 21  --> 21 Sek. bei 1 Hz
     state = (digitalRead(einHz_in));           
     //Serial.print("state: ");
     //Serial.println(state);
     //delay(300);
       
     if(laststate == HIGH && state == LOW)   // Flankenauswertung (High --> Low)
      {
         zaehler = ( zaehler + 1 );
         //Serial.print("Zaehler-up: ");
         Serial.println(zaehler);
        
        if ((zaehler <= t ) && (digitalRead(taster_licht))) // Lichttaster gedrückt --> Neustart 
          {
            digitalWrite( eltako , LOW );
            Serial.println("         Licht: ON ");
          }
        else
          {
            digitalWrite( eltako , HIGH );
            Serial.println("      Licht: OFF ");
            lichtState = LOW;
            digitalWrite(einHz_Takt , HIGH);
            zaehler = 0;
          }
      }
    laststate = state;
  }
}
